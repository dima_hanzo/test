<?php

namespace app\kernel\security;

use app\models\User;

interface AuthorizationInterface
{
    /**
     * Check user authorization
     *
     * @return bool
     */
    public function isAuth(): bool;

    /**
     * Login action
     *
     * @param string $login
     * @param string $password
     * @return bool
     */
    public function login(string $login, string $password): bool;

    /**
     * Logout action
     */
    public function logout();

    /**
     * Get authorized user
     *
     * @return User|null
     */
    public function getUser(): ?User;
}
