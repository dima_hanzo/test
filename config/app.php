<?php

use app\kernel\Env;

return [
    'url' => Env::get('APP_URL', 'http://127.0.0.1')
];
