<?php

namespace app\controllers;

use app\exceptions\InvalidRouteException;
use app\exceptions\InvalidValidatorException;
use app\kernel\http\Controller;
use app\kernel\http\Response;
use app\kernel\http\router\Route;
use app\kernel\security\Validator;
use app\models\User;

class AuthController extends Controller
{
    /**
     * @throws InvalidValidatorException
     * @throws InvalidRouteException
     */
    public function signIn()
    {
        if ($this->getAuth()->isAuth()) {
            return (Response::getInstance())->redirect('home');
        } else {
            if ($_SERVER['REQUEST_METHOD'] === Route::METHOD_POST) {
                $validator = new Validator();
                $validator->setRules([
                    'email' => [
                        'required',
                        'email',
                        'max:255'
                    ],
                    'password' => [
                        'required',
                        'min:5',
                        'max:255'
                    ]
                ]);

                $data = $validator->verified($_POST);
                $errors = $validator->getErrors();

                if (empty($errors)
                    && $this->getAuth()->login($data[User::COLUMN_EMAIL], $data[User::COLUMN_PASSWORD])) {
                    return (Response::getInstance())->redirect('home');
                } else {
                    return $this->view->render('sign-in', [
                        'form' => $data,
                        'errors' => $errors
                    ]);
                }
            } else {
                return $this->view->render('sign-in');
            }
        }
    }

    /**
     * @return bool
     * @throws InvalidRouteException
     * @throws InvalidValidatorException
     */
    public function signUp()
    {
        if ($this->getAuth()->isAuth()) {
            return (Response::getInstance())->redirect('home');
        } else {
            if ($_SERVER['REQUEST_METHOD'] === Route::METHOD_POST) {
                $validator = new Validator();
                $validator->setRules([
                    User::COLUMN_NAME => [
                        'required',
                        'max:255'
                    ],
                    User::COLUMN_EMAIL => [
                        'required',
                        'email',
                        'max:255',
                        'unique:User:email'
                    ],
                    User::COLUMN_PASSWORD => [
                        'required',
                        'min:5',
                        'max:255'
                    ],
                    'confirm_password' => [
                        'required',
                        'min:5',
                        'max:255',
                        'equal:password'
                    ]
                ]);

                $data = $validator->verified($_POST);
                $errors = $validator->getErrors();

                if (empty($errors) && $this->getAuth()->register($data)) {
                    return (Response::getInstance())->redirect('home');
                } else {
                    return $this->view->render('sign-up', [
                        'form' => $data,
                        'errors' => $errors
                    ]);
                }
            } else {
                return $this->view->render('sign-up');
            }
        }
    }

    /**
     * Do sign out action
     * @throws InvalidRouteException
     */
    public function signOut()
    {
        $this->getAuth()->logout();

        return (Response::getInstance())->redirect('home');
    }
}
