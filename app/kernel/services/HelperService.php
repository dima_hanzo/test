<?php

namespace app\kernel\services;

use app\kernel\database\Model;

class HelperService
{
    /**
     * @param string $string
     * @param bool $capitalizeFirstCharacter
     * @return string
     */
    public function snakeToCamelCase(string $string, bool $capitalizeFirstCharacter = false): string
    {
        $result = str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));

        return $capitalizeFirstCharacter ? $result : lcfirst($result);
    }

    /**
     * @param string $string
     * @return string
     */
    public function camelCaseToSnake(string $string): string
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $string, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }

    /**
     * @param Model $model
     * @return array
     */
    public function transformModelToArray(Model $model): array
    {
        $result = [];

        $getters = array_filter(get_class_methods($model), function ($methodName) {
            return !is_bool(strpos($methodName, 'get'));
        });

        foreach ($getters as $getter) {
            $column = $this->camelCaseToSnake(lcfirst(str_replace('get', '', $getter)));
            $result[$column] = $model->$getter();
        }

        return $result;
    }
}
