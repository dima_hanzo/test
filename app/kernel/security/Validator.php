<?php

namespace app\kernel\security;

use app\exceptions\InvalidValidatorException;
use app\exceptions\ValidatorException;
use app\kernel\database\Model;

final class Validator
{
    /**
     * @var array $errors
     */
    private $errors = [];

    /**
     * @var array $rules
     */
    private $rules = [];

    /**
     * @var string $rule
     */
    private $rule;
    /**
     * @var array $validators
     */
    private $validators = [];

    /**
     * @var array $data
     */
    private $data;

    public function __construct()
    {
        $this->prepareValidators();
    }

    /**
     * @param array $rules
     */
    public function setRules(array $rules)
    {
        $this->rules = $rules;
    }

    /**
     * @param array $data
     * @return array
     * @throws InvalidValidatorException
     */
    public function verified(array $data): array
    {
        $result = [];
        $this->data = $data;

        foreach ($data as $key => $value) {
            $preparedValue = $this->prepareData($value);

            if (isset($this->rules[$key]) && !empty($this->rules[$key]) && is_array($this->rules[$key])) {
                foreach ($this->rules[$key] as $rule) {
                    $this->rule = $rule;
                    preg_match('/[a-z]+/i', $rule, $matches);
                    $ruleWithoutArgs = is_array($matches) ? reset($matches) : $matches;
                    if (array_key_exists($ruleWithoutArgs, $this->validators)) {
                        $validator = $this->validators[$ruleWithoutArgs];

                        try {
                            $this->$validator($preparedValue, $key);
                            $result[$key] = $preparedValue;
                        } catch (ValidatorException $exception) {
                            $this->errors[$key][] = $exception->getMessage();
                        }
                    } else {
                        throw new InvalidValidatorException('Unknown validator given:' . $this->rules[$key]);
                    }
                }
            } else {
                $result[$key] = $preparedValue;
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param string $email
     * @param string $key
     * @throws ValidatorException
     */
    private function validateEmail(string $email, string $key)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new ValidatorException("$key field has invalid format of email!");
        }
    }

    /**
     * @param $data
     * @param string $key
     * @throws ValidatorException
     */
    private function validateRequired($data, string $key)
    {
        if (empty($data)) {
            throw new ValidatorException("$key field is required!");
        }
    }

    /**
     * @param $data
     * @param string $key
     * @throws ValidatorException|InvalidValidatorException
     */
    private function validateMin($data, string $key)
    {
        $min = $this->parseValidatorArgument();
        if (is_numeric($data) && $data < $min) {
            throw new ValidatorException("$key field minimum value is $min!");
        } else {
            if (is_string($data) && strlen($data) < $min) {
                throw new ValidatorException("$key field minimum length is $min!");
            }
        }
    }

    /**
     * @param $data
     * @param string $key
     * @throws ValidatorException|InvalidValidatorException
     */
    private function validateMax($data, string $key)
    {
        $max = $this->parseValidatorArgument();

        if (strlen(strval($data)) > $max) {
            throw new ValidatorException("$key field maximum length is $max!");
        }
    }

    /**
     * @param $data
     * @param string $key
     * @throws ValidatorException
     * @throws InvalidValidatorException
     */
    private function validateUnique($data, string $key)
    {
        preg_match_all('/\w+/mi', $this->rule, $matches);

        if (count($matches) === 1 && count(reset($matches)) === 3) {
            $modelClass = '\\app\\models\\' . ucfirst(reset($matches)[1]);
            $column = strtolower(reset($matches)[2]);

            if (is_subclass_of($modelClass, Model::class)) {
                /**
                 * @var Model $model
                 */
                $model = new $modelClass();

                if (in_array($column, $model->getColumns())) {
                    $existedModel = $model->findOneBy([$column => $data]);

                    if (!is_null($existedModel)) {
                        throw new ValidatorException("This value already exist!");
                    }
                } else {
                    throw new InvalidValidatorException('Invalid validator argument given: ' . $this->rule);
                }
            } else {
                throw new InvalidValidatorException('Invalid validator argument given: ' . $this->rule);
            }
        } else {
            throw new InvalidValidatorException('Invalid validator argument given: ' . $this->rule);
        }
    }

    /**
     * @param $data
     * @param string $key
     * @throws ValidatorException
     * @throws InvalidValidatorException
     */
    private function validateEqual($data, string $key)
    {
        preg_match_all('/\w+/mi', $this->rule, $matches);

        if (count($matches) === 1 && count(reset($matches)) === 2) {
            $field = strtolower(end($matches[0]));

            if (isset($this->data[$field])) {
                if ($this->data[$field] !== $data) {
                    throw new ValidatorException("$key field value must be same $field value!");
                }
            } else {
                throw new ValidatorException("$key field value must be same $field value!");
            }
        } else {
            throw new InvalidValidatorException('Invalid validator argument given: ' . $this->rule);
        }
    }

    /**
     * @param string $data
     * @return string
     */
    private function prepareData(string $data): string
    {
        return htmlspecialchars(stripslashes(trim($data)));
    }

    /**
     * Prepare allowed validators
     */
    private function prepareValidators()
    {
        foreach (get_class_methods($this) as $method) {
            if (!is_bool(strpos($method, 'validate'))) {
                $this->validators[lcfirst(str_replace('validate', '', $method))] = $method;
            }
        }
    }

    /**
     * @return int
     * @throws InvalidValidatorException
     */
    private function parseValidatorArgument(): int
    {
        $result = preg_replace('/[^\d+]/i', '', $this->rule);

        if (empty($result)) {
            throw new InvalidValidatorException('Invalid validator argument given: ' . $this->rule);
        }

        return $result;
    }
}
