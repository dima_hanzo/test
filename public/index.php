<?php
require_once '../vendor/autoload.php';

use app\kernel\Application;

define('ROOT_PATH', $_SERVER['DOCUMENT_ROOT'] . '/../');

$app = new Application();
$app->init();
