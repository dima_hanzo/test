<?php

namespace app\kernel;

final class View
{
    const DIR_VIEWS = 'resources/views';
    const DIR_LAYOUTS = 'layouts';

    const VIEW_FILE_EXTENSION = '.php';

    const PATH_VIEWS = ROOT_PATH . self::DIR_VIEWS . DIRECTORY_SEPARATOR;
    const PATH_BASE_LAYOUT = self::PATH_VIEWS . 'layout' . self::VIEW_FILE_EXTENSION;

    /**
     * @var string $view
     */
    private $view;

    /**
     * @var string $layout
     */
    private $layout;

    /**
     * @var string $vars
     */
    private $vars;

    /**
     * View constructor.
     * @param string $controllerName
     * @param string $layout
     * @param array $vars
     */
    public function __construct(string $controllerName, string $layout, array $vars = [])
    {
        $this->layout = $layout;
        $parts = preg_split('/(?=[A-Z])/', $controllerName, -1, PREG_SPLIT_NO_EMPTY);
        array_pop($parts);
        $this->view = strtolower(implode('-', $parts));
        $this->vars = $vars;
    }

    /**
     * @param string $name
     * @param array $variables
     * @return bool
     */
    public function render(string $name, array $variables = []): bool
    {
        foreach ($variables as $variableName => $variable) {
            $this->setVar($variableName, $variable);
        }
        unset($variables, $variableName, $variable);

        $layoutPath = self::PATH_VIEWS . $this->view . DIRECTORY_SEPARATOR . self::DIR_LAYOUTS . DIRECTORY_SEPARATOR .
            $this->layout . self::VIEW_FILE_EXTENSION;
        $content = self::PATH_VIEWS . $this->view . DIRECTORY_SEPARATOR . $name . self::VIEW_FILE_EXTENSION;

        if (!file_exists($layoutPath)) {
            $layoutPath = self::PATH_BASE_LAYOUT;
        }

        if (!file_exists($layoutPath)) {
            trigger_error('Layout `' . $layoutPath . '` does not exist.', E_USER_NOTICE);
            return false;
        }

        if (!file_exists($content)) {
            trigger_error('Template `' . $content . '` does not exist.', E_USER_NOTICE);
            return false;
        }

        foreach ($this->vars as $key => $value) {
            $$key = $value;
        }

        unset($this->vars, $key, $value);

        include($layoutPath);

        return true;
    }

    /**
     * @param string $name
     * @param $value
     * @return bool
     */
    private function setVar(string $name, $value): bool
    {
        if (isset($this->vars[$name]) == true) {
            trigger_error('Unable to set var `' . $name . '`. Already set, and overwrite not allowed.', E_USER_NOTICE);
            return false;
        }

        $this->vars[$name] = $value;

        return true;
    }
}
