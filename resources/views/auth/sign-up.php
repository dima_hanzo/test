<form class="form-signin text-center" action="/sign-up" method="post">
    <h1 class="h3 mb-3 font-weight-normal">Register</h1>
    <input type="text"
           name="name"
           class="form-control"
           placeholder="Name"
           required
        <?php if (isset($form['name'])) { ?>
            value="<?= $form['name']; ?>"
        <?php } ?>
           autofocus>
    <?php if (isset($errors['name'])) { ?>
        <span class="error">* <?= reset($errors['name']); ?></span>
    <?php } ?>
    <input type="email"
           name="email"
           class="form-control"
           placeholder="Email address"
        <?php if (isset($form['email'])) { ?>
            value="<?= $form['email']; ?>"
        <?php } ?>
           required>
    <?php if (isset($errors['email'])) { ?>
        <span class="error">* <?= reset($errors['email']); ?></span>
    <?php } ?>
    <input type="password"
           name="password"
           class="form-control"
           placeholder="Password"
           required>
    <?php if (isset($errors['password'])) { ?>
        <span class="error">* <?= reset($errors['password']); ?></span>
    <?php } ?>
    <input type="password"
           name="confirm_password"
           class="form-control"
           placeholder="Confirm Password"
           required>
    <?php if (isset($errors['confirm_password'])) { ?>
        <span class="error">* <?= reset($errors['confirm_password']); ?></span>
    <?php } ?>
    <br/>
    <span>Already have an account? <a href="/sign-in">Sign In!</a></span>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign Up</button>
</form>
