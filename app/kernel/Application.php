<?php

namespace app\kernel;

use app\kernel\http\router\Router;
use Symfony\Component\Dotenv\Dotenv;

final class Application
{
    /**
     * Initialize application
     * @throws \ReflectionException
     */
    public function init()
    {
        $this->bootstrap();
        $router = new Router();
        $router->resolve();
    }

    /**
     * Bootstrap dependencies
     */
    public function bootstrap()
    {
        (new Dotenv())->load(ROOT_PATH . '.env');
    }
}
