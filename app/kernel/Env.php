<?php

namespace app\kernel;

final class Env
{
    /**
     * @param string $value
     * @param string|null $default
     * @return string|int|bool|null
     */
    final public static function get(string $value, string $default = null)
    {
        return isset($_ENV[$value]) ? $_ENV[$value] : $default;
    }
}
