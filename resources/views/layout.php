<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>TO DO List</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/fontawesome.min.css" rel="stylesheet">
    <link href="/css/jquery.datetimepicker.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/css/style.css" rel="stylesheet">
    <script src="/js/jquery.min.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/moment.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
<body>
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <h5 class="my-0 mr-md-auto font-weight-normal"><a href="/" class="text-dark">TO DO List</a></h5>
    <?php if ($auth['isAuth']) { ?>
        <nav class="my-2 my-md-0 mr-md-3">
            <span class="p-2 text-dark">Welcome, <?= $auth['user']['name'] ?></span>
        </nav>
        <nav class="my-2 my-md-0 mr-md-3">
            <a class="p-2 text-dark" href="/tasks">My TO DOs</a>
        </nav>
    <? } ?>

    <?php if ($auth['isAuth']) { ?>
        <a class="btn btn-outline-primary" href="/sign-out">Logout</a>
    <? } else { ?>
        <a class="btn btn-outline-primary" href="/sign-in">Sign In</a>
    <? } ?>
</div>

<div class="container">
    <?php include($content); ?>
</div>
</body>
</html>
