<?php

namespace app\controllers;

use app\exceptions\InvalidRouteException;
use app\exceptions\InvalidValidatorException;
use app\kernel\http\Controller;
use app\kernel\http\Response;
use app\kernel\http\router\Route;
use app\kernel\security\Validator;
use app\kernel\services\HelperService;
use app\models\Task;
use app\models\User;

class TaskController extends Controller
{
    /**
     * Task list
     */
    public function index()
    {
        $tasks = (new Task())->fetchTasksByUser($this->getAuth()->getUser(), true);

        return $this->view->render('index', ['tasks' => $tasks]);
    }

    /**
     * @return bool
     * @throws InvalidRouteException
     * @throws InvalidValidatorException
     */
    public function create()
    {
        $tasks = (new Task())->fetchTasksByUser($this->getAuth()->getUser());

        if ($_SERVER['REQUEST_METHOD'] === Route::METHOD_POST) {
            $validator = new Validator();
            $validator->setRules([
                Task::COLUMN_TITLE => [
                    'required',
                    'max:255',
                ],
                Task::COLUMN_BODY => [
                    'required',
                    'min:5',
                    'max:1000'
                ],
                Task::COLUMN_DUE_DATE => [
                    'required',
                ],
            ]);

            $data = $validator->verified($_POST);
            $errors = $validator->getErrors();

            if (empty($errors)) {
                $data[Task::COLUMN_USER_ID] = $this->getAuth()->getUser()->getId();
                $task = (new Task())->create($data);
                $task->onChangeChildrenUpdateStatus();

                return (Response::getInstance())->redirect('task_index');
            } else {
                return $this->view->render('create', [
                    'form' => $data,
                    'errors' => $errors,
                    'tasks' => $tasks
                ]);
            }
        } else {
            return $this->view->render('create', ['tasks' => $tasks]);
        }
    }

    /**
     * @param Task $task
     * @return bool
     * @throws InvalidRouteException
     * @throws InvalidValidatorException
     */
    public function update(Task $task)
    {
        if ($this->getAuth()->getUser()->getId() === $task->getUserId()) {
            $tasks = $task->fetchTasksByUser($this->getAuth()->getUser(), false, [$task->getId()]);
            $helper = new HelperService();

            if ($_SERVER['REQUEST_METHOD'] === Route::METHOD_POST) {
                $validator = new Validator();
                $validator->setRules([
                    Task::COLUMN_TITLE => [
                        'required',
                        'max:255',
                    ],
                    Task::COLUMN_BODY => [
                        'required',
                        'min:5',
                        'max:1000'
                    ],
                    Task::COLUMN_DUE_DATE => [
                        'required',
                    ],
                ]);

                $data = $validator->verified($_POST);
                $errors = $validator->getErrors();

                if (empty($errors)) {
                    $task->update($data);

                    return (Response::getInstance())->redirect('task_index');
                } else {
                    return $this->view->render('update', [
                        'form' => $data,
                        'errors' => $errors,
                        'tasks' => $tasks
                    ]);
                }
            } else {
                $data = $helper->transformModelToArray($task);
                return $this->view->render('update', [
                    'tasks' => $tasks,
                    'form' => $data
                ]);
            }
        } else {
            return (Response::getInstance())->forbidden();
        }
    }

    /**
     * @param Task $task
     * @throws InvalidRouteException
     */
    public function delete(Task $task)
    {
        if ($this->getAuth()->getUser()->getId() === $task->getUserId()) {
            $task->onChangeChildrenUpdateStatus(true);
            $task->delete();
            return (Response::getInstance())->redirect('task_index');
        } else {
            return (Response::getInstance())->forbidden();
        }
    }

    /**
     * @param Task $task
     * @throws InvalidRouteException
     */
    public function updateStatus(Task $task)
    {
        if ($this->getAuth()->getUser()->getId() === $task->getUserId()) {
            $task->updateStatus(!$task->getCompleted());
            return (Response::getInstance())->redirect('task_index');
        } else {
            return (Response::getInstance())->forbidden();
        }
    }

    /**
     * Do sign out action
     * @throws InvalidRouteException
     */
    public function signOut()
    {
        $this->getAuth()->logout();

        return (Response::getInstance())->redirect('home');
    }
}
