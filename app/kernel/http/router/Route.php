<?php

namespace app\kernel\http\router;

final class Route
{
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';
    const METHOD_PUT = 'PUT';
    const METHOD_PATCH = 'PATCH';
    const METHOD_DELETE = 'DELETE';

    const ALLOWED_METHODS = [
        self::METHOD_GET,
        self::METHOD_POST,
        self::METHOD_PUT,
        self::METHOD_PATCH,
        self::METHOD_DELETE,
    ];

    /**
     * @var string $pattern
     */
    private $path;

    /**
     * @var array $methods
     */
    private $methods;

    /**
     * @var string $controller
     */
    private $controller;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $action
     */
    private $action;

    /**
     * @var string $auth
     */
    private $auth;

    public function __construct(
        string $path,
        array $methods,
        string $controller,
        string $name,
        string $action = 'index',
        bool $auth = false
    ) {
        $this->setPath($path);
        $this->setMethods($methods);
        $this->setController($controller);
        $this->setName($name);
        $this->setAction($action);
        $this->setAuth($auth);
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return array
     */
    public function getMethods(): array
    {
        return $this->methods;
    }

    /**
     * @return string
     */
    public function getController(): string
    {
        return $this->controller;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @return string
     */
    public function getAuth(): string
    {
        return $this->auth;
    }

    /**
     * @param string $action
     */
    private function setAction(string $action): void
    {
        $this->action = $action;
    }

    /**
     * @param string $controller
     */
    private function setController(string $controller): void
    {
        $this->controller = $controller;
    }

    /**
     * @param array $methods
     */
    private function setMethods(array $methods): void
    {
        $this->methods = $methods;
    }

    /**
     * @param string $path
     */
    private function setPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @param string $auth
     */
    private function setAuth(string $auth): void
    {
        $this->auth = $auth;
    }

    /**
     * @param string $name
     */
    private function setName(string $name): void
    {
        $this->name = $name;
    }
}
