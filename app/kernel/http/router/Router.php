<?php

namespace app\kernel\http\router;

use app\exceptions\InvalidRouteException;
use app\kernel\database\Model;
use app\kernel\http\Controller;
use app\kernel\http\Response;
use Exception;
use ReflectionClass;

final class Router
{
    const PATH_ROUTES = ROOT_PATH . 'resources/routes/web.php';
    const CONTROLLER_NAMESPACE = 'app\controllers';

    const FIELD_ARGS = 'args';
    const FIELD_ROUTE = 'route';

    /**
     * @var array $routes
     */
    private $routes = [];

    /**
     * Router constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->initRoutes();
    }

    /**
     * Resolve route
     * @throws \ReflectionException
     */
    public function resolve()
    {
        $response = Response::getInstance();
        $response->setRoutes($this->routes);

        $found = $this->getRouteByUri();

        if ($found[self::FIELD_ROUTE]) {
            $args = $found[self::FIELD_ARGS];
            $route = $found[self::FIELD_ROUTE];

            if (in_array($_SERVER['REQUEST_METHOD'], $route->getMethods())) {
                $class = $route->getController();
                $action = $route->getAction();

                $reflectionClass = new ReflectionClass($class);
                $reflectionMethod = $reflectionClass->getMethod($action);
                foreach ($reflectionMethod->getParameters() as $param) {
                    $argumentClass = $param->getClass();

                    if ($argumentClass && is_subclass_of($argumentClass->getName(), Model::class)) {
                        $argumentInstance = ($argumentClass->newInstance())->find($args[$param->getName()]);
                        if ($argumentInstance) {
                            $args[$param->getName()] = $argumentInstance;
                        } else {
                            $response->notFound();
                        }
                    }
                }

                $controller = $reflectionClass->newInstance($route);
                $reflectionMethod->invokeArgs($controller, $args);
            } else {
                $response->notAllowed();
            }
        } else {
            $response->notFound();
        }
    }

    /**
     * @throws Exception
     */
    private function initRoutes()
    {
        if (is_readable(self::PATH_ROUTES)) {
            $this->validateRoutes(require(self::PATH_ROUTES));
        } else {
            throw new Exception('Can\'t read routes.');
        }
    }

    /**
     * @return array|null
     */
    private function getRouteByUri(): ?array
    {
        $result = [
            self::FIELD_ARGS => [],
            self::FIELD_ROUTE => null
        ];

        $url = parse_url($_SERVER['REQUEST_URI']);
        $requestUri = $url['path'];

        if (strlen($requestUri) > 1 && substr($requestUri, -1) === '/') {
            $requestUri = substr($requestUri, 0, -1);
        }


        /**
         * @var Route $route
         */
        foreach ($this->routes as $route) {
            preg_match_all(
                '/' . str_replace('/', '\/', $route->getPath()) . '/i',
                $requestUri,
                $requestMatches
            );
            if (isset($requestMatches[0][0]) && $requestMatches[0][0] === $requestUri) {
                foreach ($requestMatches as $key => $match) {
                    if (is_string($key) && !empty($match)) {
                        $result[self::FIELD_ARGS][$key] = reset($match);
                    }
                }
                $result[self::FIELD_ROUTE] = $route;
                break;
            }
        }

        return $result;
    }

    /**
     * @param array $routes
     * @throws InvalidRouteException
     * @throws \ReflectionException
     */
    private function validateRoutes(array $routes)
    {
        foreach ($routes as $route) {
            if ($route instanceof Route) {
                if ($this->isValidRoute($route)) {
                    $this->routes[$route->getName()] = $route;
                }
            } else {
                throw new InvalidRouteException('Route should be instance of ' . Route::class);
            }
        }
    }

    /**
     * @param Route $route
     * @return bool
     * @throws InvalidRouteException
     * @throws \ReflectionException
     */
    private function isValidRoute(Route $route): bool
    {
        if (isset($this->routes[$route->getName()])) {
            throw new InvalidRouteException(
                'Route with same name: ' . $route->getName() . ' is already exist'
            );
        }

        if (!empty(array_filter($route->getMethods(), function ($routeMethod) {
            return !in_array($routeMethod, Route::ALLOWED_METHODS);
        }))) {
            throw new InvalidRouteException(
                'Unknown route method given' . $route->getMethods() .
                ' allowed methods: ' . implode(', ', Route::ALLOWED_METHODS)
            );
        }

        $controllerClass = new \ReflectionClass($route->getController());

        if (!$controllerClass->isSubclassOf(Controller::class)
            || !$controllerClass->getNamespaceName() === self::CONTROLLER_NAMESPACE) {
            throw new InvalidRouteException('Unknown controller given ' . $controllerClass->getName());
        }

        if (!$controllerClass->hasMethod($route->getAction())) {
            throw new InvalidRouteException(
                'Unknown controller action given ' . $route->getAction() . ' in ' . $controllerClass->getName()
            );
        }

        return true;
    }
}
