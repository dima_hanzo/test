<form class="form-signin text-center" action="/sign-in" method="post">
    <h1 class="h3 mb-3 font-weight-normal">Sign In</h1>
    <input type="email"
           name="email"
           class="form-control"
           placeholder="Email address"
        <?php if (isset($form['email'])) { ?>
            value="<?= $form['email']; ?>"
        <?php } ?>
           required
           autofocus>
    <?php if (isset($errors['email'])) { ?>
        <span class="error">* <?= reset($errors['email']); ?></span>
    <?php } ?>
    <input type="password"
           name="password"
           class="form-control"
           placeholder="Password"
           required>
    <?php if (isset($errors['password'])) { ?>
        <span class="error">* <?= reset($errors['password']); ?></span>
    <?php } ?>
    <br/>
    <span>Don't have an account? <a href="/sign-up">Sign Up!</a></span>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
</form>
