<?php

namespace app\controllers;

use app\kernel\http\Controller;

class HomeController extends Controller
{
    /**
     * Render home page
     */
    public function index()
    {
        $this->view->render('index');
    }
}
