<?php

namespace app\kernel\security;

use app\models\User;

final class Authorization implements AuthorizationInterface
{
    const FIELD_IS_AUTH = 'is_auth';
    const FIELD_LOGIN = 'login';

    /**
     * @var User|null $user
     */
    private $user;

    /**
     * Authorization constructor.
     */
    public function __construct()
    {
        $this->resolveAuth();
    }

    /**
     * Check user authorization
     *
     * @return bool
     */
    public function isAuth(): bool
    {
        return isset($_SESSION[self::FIELD_IS_AUTH])
            && !empty($_SESSION[self::FIELD_IS_AUTH])
            && $_SESSION[self::FIELD_IS_AUTH];
    }

    /**
     * Login action
     *
     * @param string $login
     * @param string $password
     * @return bool
     */
    public function login(string $login, string $password): bool
    {
        $_SESSION[self::FIELD_IS_AUTH] = false;
        $user = (new User())->findOneBy([User::COLUMN_EMAIL => $login]);

        if ($user) {
            if ($user->checkPassword($password)) {
                $_SESSION[self::FIELD_IS_AUTH] = true;
                $_SESSION[self::FIELD_LOGIN] = $user->getEmail();
                $this->user = $user;
            }
        }

        return $_SESSION[self::FIELD_IS_AUTH];
    }

    /**
     * @param array $data
     * @return bool
     */
    public function register(array $data): bool
    {
        $plainPassword = $data[User::COLUMN_PASSWORD];
        $data[User::COLUMN_PASSWORD] = password_hash($plainPassword, PASSWORD_DEFAULT);
        (new User())->create($data);
        return $this->login($data[User::COLUMN_EMAIL], $plainPassword);
    }

    /**
     * Logout action
     */
    public function logout()
    {
        $_SESSION = [];
        session_destroy();
    }

    /**
     * Get authorized user
     *
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->isAuth() ? $this->user : null;
    }

    /**
     * Resolve user auth
     */
    private function resolveAuth()
    {
        session_start();

        $user = null;

        if ($this->isAuth()) {
            $user = (new User())->findOneBy([User::COLUMN_EMAIL => $_SESSION[self::FIELD_LOGIN]]);
        }

        $this->user = $user;
    }
}
