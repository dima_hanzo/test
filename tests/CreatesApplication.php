<?php

namespace tests;

use app\kernel\Application;

trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return Application
     */
    public function createApplication()
    {
        define('ROOT_PATH', __DIR__ . '/../');

        $app = new Application();

        $app->bootstrap();

        return $app;
    }
}
