<?php

use app\kernel\Env;

return [
    'connection' => Env::get('DB_CONNECTION', 'mysql'),
    'mysql' => [
        'prefix' => 'mysql',
        'host' => Env::get('DB_HOST', '127.0.0.1'),
        'port' => Env::get('DB_PORT', 3306),
        'database' => Env::get('DB_DATABASE', 'default'),
        'user' => Env::get('DB_USERNAME', 'user'),
        'password' => Env::get('DB_PASSWORD', 'secret'),
        'charset' => Env::get('DB_CHARSET', 'utf8mb4'),
        'collation' => Env::get('DB_COLLATION', 'utf8mb4_unicode_ci'),
    ],
];
