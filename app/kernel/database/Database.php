<?php

namespace app\kernel\database;

use app\kernel\Config;
use app\kernel\Singleton;
use PDO;

final class Database extends Singleton
{
    /**
     * @var PDO
     */
    public $connection;

    /**
     * @var string
     */
    private $credentials;

    /**
     * @var Config
     */
    private $config;

    /**
     * Connection constructor.
     */
    protected function __construct()
    {
        $this->config = $config = Config::getInstance();
        $this->credentials = $this->prepareDSN();

        $defaultDB = $this->config->getValue('database.connection');

        $this->connection = new PDO(
            $this->credentials,
            $this->config->getValue("database.$defaultDB.user"),
            $this->config->getValue("database.$defaultDB.password")
        );
        $this->connection->exec(
            "SET NAMES '" . $this->config->getValue("database.$defaultDB.charset") .
            "' COLLATE '" . $this->config->getValue("database.$defaultDB.collation") . "'"
        );
        $this->connection->exec(
            "SET CHARACTER SET '" . $this->config->getValue("database.$defaultDB.charset") . "'"
        );
        $this->connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
    }

    /**
     * @return string
     */
    final private function prepareDSN(): string
    {
        $defaultDB = $this->config->getValue('database.connection');
        $prefix = $this->config->getValue("database.$defaultDB.prefix");
        $host = $this->config->getValue("database.$defaultDB.host");
        $port = $this->config->getValue("database.$defaultDB.port");
        $db = $this->config->getValue("database.$defaultDB.database");

        return "$prefix:host=$host;port=$port;dbname=$db";
    }
}
