<?php

namespace app\kernel\database;

use app\exceptions\InvalidModelException;
use app\kernel\services\HelperService;
use PDO;

abstract class Model
{
    /**
     * @var string $tableName
     */
    protected $tableName;

    /**
     * @var array $columns
     */
    protected $columns;

    /**
     * @var QueryBuilder $queryBuilder
     */
    protected $queryBuilder;

    /**
     * @var string $primary
     */
    protected $primary = 'id';

    /**
     * Model constructor.
     * @throws InvalidModelException
     */
    final public function __construct()
    {
        $this->queryBuilder = new QueryBuilder();
        $this->check();
    }

    /**
     * @return array
     */
    final public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @return string
     */
    final protected function getTableName(): string
    {
        return $this->tableName;
    }

    /**
     * @return QueryBuilder
     */
    final protected function getQueryBuilder(): QueryBuilder
    {
        return $this->queryBuilder;
    }

    /**
     * Check model properties
     * @throws InvalidModelException
     */
    final private function check()
    {
        if (!$this->tableName || !is_string($this->tableName)) {
            throw new InvalidModelException('Property $tableName must be defined as string!');
        }

        if (!$this->columns || !is_array($this->columns)) {
            throw new InvalidModelException('Property $columns must be defined as array!');
        } else {
            foreach ($this->columns as $column) {
                if (!is_string($column)) {
                    throw new InvalidModelException('Columns must be type of string!');
                }
            }
        }
    }

    /**
     * @param $id
     * @return Model|null
     */
    final public function find($id): ?self
    {
        return $this->findOneBy([$this->primary => $id]);
    }

    /**
     * @param array $conditions
     * @param int|null $limit
     * @return mixed
     */
    final public function findBy(array $conditions, int $limit = null)
    {
        return $this->queryBuilder->table($this->tableName)->where($conditions)->limit($limit)->getAll('array');
    }

    /**
     * @param array $conditions
     * @return $this|null
     */
    final public function findOneBy(array $conditions): ?self
    {
        $result = $this->findBy($conditions, 1);

        if (!empty($result)) {
            $this->fill(reset($result));
        }

        return empty($result) ? null : $this;
    }

    /**
     * @param string $orderBy
     * @param string $sort
     * @return array|null
     */
    final public function all(string $orderBy = 'id', string $sort = 'ASC'): ?array
    {
        return $this->queryBuilder->table($this->tableName)->orderBy($orderBy, $sort)->getAll('array');
    }

    /**
     * @param array $data
     * @return $this|null
     */
    final public function create(array $data): ?self
    {
        $data[$this->primary] = $this->queryBuilder->table($this->tableName)->insert($this->prepareData($data));
        $this->fill($data);

        return $data[$this->primary] ? $this : null;
    }

    /**
     * @param array $data
     * @return $this|null
     */
    final public function update(array $data): ?self
    {
        if ($data[$this->primary]) {
            $this->queryBuilder->table($this->tableName)
                ->where($this->primary, '=', $data[$this->primary])
                ->update($this->prepareData($data));
            $this->fill($data);
        }

        return $data[$this->primary] ? $this : null;
    }

    /**
     * Delete model
     *
     * @return null
     */
    final public function delete()
    {
        $primary = $this->primary;
        if ($primary) {
            $this->queryBuilder->table($this->tableName)
                ->where($primary, '=', $this->$primary)
                ->delete();
        }

        return null;
    }

    /**
     * @param array $data
     * @param bool $clear
     */
    final private function fill(array $data, bool $clear = false)
    {
        $helper = new HelperService();

        foreach ($this->columns as $column) {
            $property = $helper->snakeToCamelCase($column);

            if (property_exists($this, $property)) {
                $this->$property = $clear ? null : $data[$column] === 'null' ? null : $data[$column];
            }
        }
    }

    /**
     * @param array $data
     * @return array
     */
    private function prepareData(array $data): array
    {
        $result = [];

        foreach ($data as $column => $value) {
            if (in_array($column, $this->columns)) {
                $result[$column] = $data[$column] === 'null' ? null : $data[$column];
            }
        }

        return $result;
    }

}
