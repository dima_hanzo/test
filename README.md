## Install

- `cp .env.example .env` and add your DB credentials and app URL
- `docker-compose up -d`
- `composer install`
- `sudo docker exec -i [container-id] mysql -h 192.169.220.1 -P 3307 -uroot -p123456 tododb < db.sql` to import `db.sql` from root directory

## PHPUnit
- `docker-compose exec app php ./vendor/bin/phpunit tests`

## .env example
- APP_URL='http://127.0.0.1:8001'
- DB_CONNECTION=mysql
- DB_HOST=192.169.220.1
- DB_PORT=3307
- DB_DATABASE=tododb
- DB_USERNAME=tododb
- DB_PASSWORD=12345
