<?php

namespace tests;

use app\models\User;

class UserTest extends TestCase
{
    public function testCreateUser()
    {
        $data = [
            User::COLUMN_EMAIL => rand(9999, 9999999) . time() . '@test.com',
            User::COLUMN_NAME => 'test',
            User::COLUMN_PASSWORD => password_hash('test', PASSWORD_DEFAULT),
        ];

        $user = new User();
        $user->create($data);

        $this->assertStringContainsString($data[User::COLUMN_EMAIL], $user->getEmail());
        $this->assertStringContainsString($data[User::COLUMN_NAME], $user->getName());
        $this->assertTrue($user->checkPassword('test'));
    }
}
