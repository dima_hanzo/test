<?php

namespace app\models;

use app\kernel\database\Model;

class Task extends Model
{
    const COLUMN_ID = 'id';
    const COLUMN_TITLE = 'title';
    const COLUMN_BODY = 'body';
    const COLUMN_DUE_DATE = 'due_date';
    const COLUMN_COMPLETED = 'completed';
    const COLUMN_PARENT_ID = 'parent_id';
    const COLUMN_USER_ID = 'user_id';
    const COLUMN_UPDATED = 'updated_at';
    const COLUMN_CREATED_AT = 'created_at';

    /**
     * @var string $tableName
     */
    protected $tableName = 'tasks';

    /**
     * @var array $columns
     */
    protected $columns = [
        self::COLUMN_ID,
        self::COLUMN_TITLE,
        self::COLUMN_BODY,
        self::COLUMN_DUE_DATE,
        self::COLUMN_COMPLETED,
        self::COLUMN_PARENT_ID,
        self::COLUMN_USER_ID,
        self::COLUMN_UPDATED,
        self::COLUMN_CREATED_AT,
    ];

    /**
     * @var int $id
     */
    protected $id;

    /**
     * @var string $title
     */
    protected $title;

    /**
     * @var string $body
     */
    protected $body;

    /**
     * @var string $dueDate
     */
    protected $dueDate;

    /**
     * @var bool $completed
     */
    protected $completed;
    /**
     * @var int|null $parentId
     */
    protected $parentId;

    /**
     * @var int $userId
     */
    protected $userId;

    /**
     * @var string $updatedAt
     */
    protected $updatedAt;

    /**
     * @var string $createdAt
     */
    protected $createdAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getBody(): ?string
    {
        return $this->body;
    }

    /**
     * @return string
     */
    public function getDueDate(): ?string
    {
        return $this->dueDate;
    }

    /**
     * @return bool
     */
    public function getCompleted(): ?bool
    {
        return boolval($this->completed);
    }

    /**
     * @return int|null
     */
    public function getParentId(): ?int
    {
        return $this->parentId;
    }

    /**
     * @return int
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): ?string
    {
        return $this->updatedAt;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    /**
     * @param User $user
     * @param bool $groupByParent
     * @param array $ignoreIds
     * @return array
     */
    public function fetchTasksByUser(User $user, bool $groupByParent = false, array $ignoreIds = []): array
    {
        $result = [];
        $qb = $this->getQueryBuilder()
            ->table($this->tableName)
            ->where(self::COLUMN_USER_ID, '=', $user->getId());

        if (!empty($ignoreIds)) {
            $qb->notIn(self::COLUMN_ID, $ignoreIds);
        }

        if (!$groupByParent) {
            $qb->whereNull(self::COLUMN_PARENT_ID);
        }

        $tasks = $qb->orderBy(self::COLUMN_DUE_DATE, 'ASC')
            ->getAll('array');

        if ($groupByParent) {
            foreach ($tasks as $task) {
                if ($task[self::COLUMN_PARENT_ID]) {
                    $result[$task[self::COLUMN_PARENT_ID]]['tasks'][$task[self::COLUMN_ID]] = $task;
                } else {
                    if (is_array($result[$task[self::COLUMN_ID]])) {
                        $result[$task[self::COLUMN_ID]] = array_merge($result[$task[self::COLUMN_ID]], $task);
                    } else {
                        $result[$task[self::COLUMN_ID]] = $task;
                    }
                }
            }
        } else {
            $result = $tasks;
        }

        return $result;
    }

    /**
     * Update status on delete or create children
     * @param bool $ignoreCurrent
     */
    public function onChangeChildrenUpdateStatus(bool $ignoreCurrent = false)
    {
        if ($this->getParentId()) {
            $qb = $this->getQueryBuilder()
                ->table($this->tableName)
                ->select(self::COLUMN_ID)
                ->where(self::COLUMN_PARENT_ID, '=', $this->getParentId())
                ->where(self::COLUMN_COMPLETED, '=', 0);

            if ($ignoreCurrent) {
                $qb->notWhere(self::COLUMN_ID, '=', $this->getId());
            }

            $relatedTasks = $qb->getAll('array');

            $this->getQueryBuilder()
                ->table($this->tableName)
                ->where(self::COLUMN_ID, '=', $this->getParentId())
                ->update([self::COLUMN_COMPLETED => intval(empty($relatedTasks))]);
        }
    }

    /**
     * Update status
     * @param bool $completed
     */
    public function updateStatus(bool $completed)
    {
        if ($this->getParentId()) {
            $relatedTasks = $this->getQueryBuilder()
                ->table($this->tableName)
                ->select(self::COLUMN_ID)
                ->where(self::COLUMN_PARENT_ID, '=', $this->getParentId())
                ->where(self::COLUMN_COMPLETED, '=', 0)
                ->notWhere(self::COLUMN_ID, '=', $this->getId())
                ->getAll('array');

            if (!empty($relatedTasks)) {
                $this->update([
                    self::COLUMN_ID => $this->getId(),
                    self::COLUMN_COMPLETED => intval($completed)
                ]);
            } else {
                $this->getQueryBuilder()
                    ->table($this->tableName)
                    ->in(self::COLUMN_ID, [$this->getId(), $this->getParentId()])
                    ->update([self::COLUMN_COMPLETED => intval($completed)]);
            }
        } else {
            $relatedTasks = $this->getQueryBuilder()
                ->table($this->tableName)
                ->select(self::COLUMN_ID)
                ->where(self::COLUMN_PARENT_ID, '=', $this->getId())
                ->where(self::COLUMN_COMPLETED, '=', $this->getCompleted())
                ->getAll('array');

            if (!empty($relatedTasks)) {
                $this->getQueryBuilder()
                    ->table($this->tableName)
                    ->in(self::COLUMN_ID, array_column($relatedTasks, self::COLUMN_ID))
                    ->update([self::COLUMN_COMPLETED => intval($completed)]);
            }

            $this->update([
                self::COLUMN_ID => $this->getId(),
                self::COLUMN_COMPLETED => intval($completed)
            ]);
        }
    }
}
