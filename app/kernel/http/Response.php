<?php

namespace app\kernel\http;

use app\exceptions\InvalidRouteException;
use app\kernel\Config;
use app\kernel\Singleton;

final class Response extends Singleton
{
    /**
     * @var Config $config
     */
    private $config;

    /**
     * @var array $routes
     */
    private $routes;

    /**
     * Response constructor.
     */
    protected function __construct()
    {
        $this->config = Config::getInstance();
    }

    /**
     * Response with redirect 301
     * @param string $routeName
     * @throws InvalidRouteException
     */
    public function redirect(string $routeName)
    {
        if (isset($this->routes[$routeName])) {
            $url = (Config::getInstance())->getValue('app.url') . $this->routes[$routeName]->getPath();
            header($_SERVER["SERVER_PROTOCOL"] . " 301 Moved Permanently", true, 301);
            header("Location: $url");
            die;
        } else {
            throw new InvalidRouteException('Unknown route name: ' . $routeName);
        }
    }

    /**
     * Response with code 404 Not Found
     */
    public function notFound()
    {
        header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found", true, 404);
        die('404 Not Found');
    }

    /**
     * Response with code 405 Method Not Allowed
     */
    public function notAllowed()
    {
        header($_SERVER["SERVER_PROTOCOL"] . " 405 Method Not Allowed", true, 405);
        die('405 Method Not Allowed');
    }

    /**
     * Response with code 403 Forbidden
     */
    public function forbidden()
    {
        header($_SERVER["SERVER_PROTOCOL"] . " 403 Forbidden", true, 403);
        die('403 Forbidden');
    }

    /**
     * @param array $routes
     */
    public function setRoutes(array $routes)
    {
        $this->routes = $routes;
    }
}
