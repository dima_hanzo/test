<?php

namespace app\kernel\http;

use app\exceptions\InvalidRouteException;
use app\kernel\http\router\Route;
use app\kernel\http\router\Router;
use app\kernel\security\Authorization;
use app\kernel\services\HelperService;
use app\kernel\View;
use ReflectionException;

abstract class Controller
{
    const FIELD_AUTH = 'auth';
    const FIELD_IS_AUTH = 'isAuth';
    const FIELD_USER = 'user';

    /**
     * @var View $view
     */
    protected $view;

    /**
     * @var string $layout
     */
    protected $layout = 'layout';

    /**
     * @var Authorization $auth
     */
    private $auth;

    /**
     * @var Route $route
     */
    private $route;

    /**
     * Controller constructor.
     *
     * @param Route $route
     * @throws ReflectionException|InvalidRouteException
     */
    public function __construct(Route $route)
    {
        $this->route = $route;
        $this->auth = new Authorization();
        $instance = new \ReflectionClass($this);
        $this->view = new View($instance->getShortName(), $this->layout, $this->prepareAuthData());
        $this->checkAuth();
    }

    /**
     * @return Authorization
     */
    protected function getAuth(): Authorization
    {
        return $this->auth;
    }

    /**
     * Check route authorization
     * @throws InvalidRouteException
     */
    private function checkAuth()
    {
        if ($this->route->getAuth() && !$this->auth->isAuth()) {
            (Response::getInstance())->redirect('signIn');
        }
    }

    /**
     * @return array
     */
    private function prepareAuthData(): array
    {
        $user = $_SESSION[Authorization::FIELD_IS_AUTH]
            ? (new HelperService())->transformModelToArray($this->auth->getUser())
            : null;

        return [
            self::FIELD_AUTH => [
                self::FIELD_IS_AUTH => (bool)$_SESSION[Authorization::FIELD_IS_AUTH],
                self::FIELD_USER => $user,
            ]
        ];
    }
}
