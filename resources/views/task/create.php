<form action="/tasks/create" method="post">
    <h1 class="h3 mb-3 font-weight-normal">Create a new task</h1>
    <label for="form_title">Title:</label>
    <input type="text"
           id="form_title"
           name="title"
           class="form-control"
           placeholder="Title"
        <?php if (isset($form['title'])) { ?>
            value="<?= $form['title']; ?>"
        <?php } ?>
           autofocus>
    <?php if (isset($errors['title'])) { ?>
        <span class="error">* <?= reset($errors['title']); ?></span>
    <?php } ;?>
    <br/>
    <label for="form_body">Description:</label>
    <textarea id='form_body'
              maxlength="1000"
              minlength="5"
              name="body"
              class="form-control"
              placeholder="Description"><?php if (isset($form['body'])) {echo $form['body'];}?></textarea>
    <?php if (isset($errors['body'])) { ?>
        <span class="error">* <?= reset($errors['body']); ?></span>
    <?php } ?>
    <br/>
    <label for="form_due_date">Due Date:</label>
    <input type="text"
           id="form_due_date"
           name="due_date"
           class="form-control"
           placeholder="Due Date"
        <?php if (isset($form['due_date'])) { ?>
            value="<?= $form['due_date']; ?>"
        <?php } ?>>
    <?php if (isset($errors['due_date'])) { ?>
        <span class="error">* <?= reset($errors['due_date']); ?></span>
    <?php } ?>
    <br/>
    <label for="form_parent_id">Parent task:</label>
    <select
            id="form_parent_id"
            name="parent_id"
            class="form-control">
        <option value="null">---</option>
        <?php foreach ($tasks as $task) {?>
            <option value="<?= $task['id']; ?>"
                <?php if (isset($form['parent_id'])) { ?>
                    selected
                <?php } ?>>
                <?= $task['title']; ?>
            </option>
        <?php } ?>
    </select>
    <?php if (isset($errors['parent_id'])) { ?>
        <span class="error">* <?= reset($errors['parent_id']); ?></span>
    <?php } ?>
    <br/>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
</form>
<script src="/js/jquery.datetimepicker.full.js"></script>
<script type="text/javascript">
    $( document ).ready(function() {
        $('#form_due_date').datetimepicker({
            format:'Y-m-d H:i:s',
            minDate: 0,
            minTime: 0,
        });
    })

</script>
