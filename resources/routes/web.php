<?php

use app\controllers\AuthController;
use app\controllers\TaskController;
use app\kernel\http\router\Route;
use app\controllers\HomeController;

return [
    new Route('/', ['GET'], HomeController::class, 'home', 'index', true),
    new Route('/sign-in', ['GET', 'POST'], AuthController::class, 'signIn', 'signIn'),
    new Route('/sign-up', ['GET', 'POST'], AuthController::class, 'signUp', 'signUp'),
    new Route('/sign-out', ['GET'], AuthController::class, 'signOut', 'signOut'),
    new Route('/tasks', ['GET'], TaskController::class, 'task_index', 'index', true),
    new Route('/tasks/create', ['GET', 'POST'], TaskController::class, 'task_create', 'create', true),
    new Route('/tasks/update/(?<task>[\d]+)', ['GET', 'POST'], TaskController::class, 'task_update', 'update', true),
    new Route('/tasks/delete/(?<task>[\d]+)', ['GET'], TaskController::class, 'task_delete', 'delete', true),
    new Route(
        '/tasks/update-status/(?<task>[\d]+)',
        ['GET'],
        TaskController::class,
        'task_update_status',
        'updateStatus',
        true
    ),
];
