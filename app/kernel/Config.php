<?php

namespace app\kernel;

final class Config extends Singleton
{
    /**
     * @var array
     */
    private $hashmap = [];

    /**
     * Config constructor.
     */
    protected function __construct()
    {
        $this->prepareHashmap();
    }

    /**
     * @param string $path
     * @return string|array|null
     */
    public function getValue(string $path)
    {
        $keys = explode('.', $path);
        $value = null;
        foreach ($keys as $key) {
            if (!$value) {
                $value = $this->hashmap[$key];
            } else {
                $value = $value[$key];
            }
        }
        return $value;
    }

    /**
     * @param string $path
     * @return bool
     */
    public function existValue(string $path): bool
    {
        $result = false;
        try {
            $result = boolval($this->getValue($path));
        } catch (\Exception $exception) {
        }
        return $result;
    }

    /**
     * @param string $key
     * @param string $value
     */
    private function setValue(string $key, $value): void
    {
        $this->hashmap[$key] = $value;
    }

    /**
     * Prepare hashmap
     */
    private function prepareHashmap(): void
    {
        $configDir = ROOT_PATH . '/config/';

        foreach (scandir($configDir) as $fileName) {
            if (is_file($configDir . $fileName)) {
                $file = include($configDir . $fileName);
                $this->setValue(basename($fileName, '.php'), $file);
            }
        }
    }
}
