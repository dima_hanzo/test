<div class="row">
    <div class="col-md-12">
        <div class="box box-aqua">
            <div class="box-header ui-sortable-handle" style="cursor: move;">
                <i class="ion ion-clipboard"></i>
                <h3 class="box-title">To Do List</h3>
            </div>

            <div class="box-body">
                <ul class="todo-list ui-sortable">
                    <?php if (!empty($tasks)) { ?>
                        <?php foreach ($tasks as $task) { ?>
                            <li>
                            <span class="handle ui-sortable-handle">
                                  <i class="fas fa-ellipsis-v"></i>
                                  <i class="fas fa-ellipsis-v"></i>
                                </span>
                                <input type="checkbox" <?php if (boolval($task['completed'])) { ?>
                                    checked
                                <?php } ?>
                                       name="<?= $task['id']; ?>"
                                       onchange="document.location.href=`/tasks/update-status/<?= $task['id']; ?>`;"/>
                                <span class="text"> <?= $task['title']; ?></span>
                                <small><i class="fas fa-clock"></i><?= $task['due_date']; ?></small>
                                <div class="tools">
                                    <a href="/tasks/update/<?= $task['id']; ?>"><i class="fas fa-edit"></i></a>
                                    <a href="/tasks/delete/<?= $task['id']; ?>"><i class="fas fa-trash" style="color: red"></i></a>
                                </div>
                                <?php if (!empty($task['tasks'])) { ?>
                                    <ul class="todo-list ui-sortable">
                                        <?php foreach ($task['tasks'] as $subtask) { ?>
                                            <li>
                                <span class="handle ui-sortable-handle">
                                  <i class="fas fa-ellipsis-v"></i>
                                  <i class="fas fa-ellipsis-v"></i>
                                </span>
                                                <input type="checkbox" <?php if (boolval($subtask['completed'])) { ?>
                                                    checked
                                                <?php } ?>
                                                       name="<?= $task['id']; ?>"
                                                       onchange="document.location.href=`/tasks/update-status/<?= $subtask['id']; ?>`;">
                                                <span class="text"> <?= $subtask['title']; ?></span>
                                                <small><i class="fas fa-clock"></i><?= $subtask['due_date']; ?></small>
                                                <div class="tools">
                                                    <a href="/tasks/update/<?= $subtask['id']; ?>">
                                                        <i class="fas fa-edit"></i></a>
                                                    <a href="/tasks/delete/<?= $subtask['id']; ?>">
                                                        <i class="fas fa-trash" style="color: red"></i></a>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </li>
                        <?php } ?>
                    <?php } else { ?>
                        <h3 class="text-center text-capitalize">No data</h3>
                    <?php } ?>
                </ul>
            </div>
            <div class="box-footer clearfix no-border">
                <a href="/tasks/create" class="btn btn-default pull-right"><i class="fas fa-plus"></i> Add item</a>
            </div>
        </div>
    </div>
</div>
