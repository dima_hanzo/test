<?php

namespace tests;

abstract class TestCase extends \PHPUnit\Framework\TestCase
{
    use CreatesApplication;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $app = $this->createApplication();
    }
}
