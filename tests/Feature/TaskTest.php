<?php

namespace tests;

use app\models\Task;
use app\models\User;

class TaskTest extends TestCase
{
    public function testCreateTask()
    {
        $userData = [
            User::COLUMN_EMAIL => rand(9999, 9999999) . time() . '@test.com',
            User::COLUMN_NAME => 'test',
            User::COLUMN_PASSWORD => password_hash('test', PASSWORD_DEFAULT),
        ];

        $user = new User();
        $user->create($userData);

        $taskData = [
            Task::COLUMN_TITLE => 'test title',
            Task::COLUMN_BODY => 'test body',
            Task::COLUMN_DUE_DATE => (new \DateTime())->format('Y-m-d H:i:s'),
            Task::COLUMN_USER_ID => $user->getId(),
        ];

        $task = new Task();
        $task->create($taskData);

        $this->assertStringContainsString($taskData[Task::COLUMN_TITLE], $task->getTitle());
        $this->assertStringContainsString($taskData[Task::COLUMN_BODY], $task->getBody());
        $this->assertStringContainsString($taskData[Task::COLUMN_DUE_DATE], $task->getDueDate());
        $this->assertSame($taskData[Task::COLUMN_USER_ID], $task->getUserId());
        $this->assertFalse($task->getCompleted());
    }

    public function testCreateSubTask()
    {
        $userData = [
            User::COLUMN_EMAIL => rand(9999, 9999999) . time() . '@test.com',
            User::COLUMN_NAME => 'test',
            User::COLUMN_PASSWORD => password_hash('test', PASSWORD_DEFAULT),
        ];

        $user = new User();
        $user->create($userData);

        $parentTaskData = [
            Task::COLUMN_TITLE => 'test title',
            Task::COLUMN_BODY => 'test body',
            Task::COLUMN_DUE_DATE => (new \DateTime())->format('Y-m-d H:i:s'),
            Task::COLUMN_USER_ID => $user->getId(),
        ];

        $parentTask = new Task();
        $parentTask->create($parentTaskData);

        $subTaskData = [
            Task::COLUMN_TITLE => 'test title',
            Task::COLUMN_BODY => 'test body',
            Task::COLUMN_DUE_DATE => (new \DateTime())->format('Y-m-d H:i:s'),
            Task::COLUMN_USER_ID => $user->getId(),
            Task::COLUMN_PARENT_ID => $parentTask->getId(),
        ];

        $subTask = new Task();
        $subTask->create($subTaskData);

        $this->assertSame($parentTask->getId(), $subTask->getParentId());
    }
}
